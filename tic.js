var tab = [
    [0,0,0], // rangée 1
    [0,0,0], // rangée 2
    [0,0,0]  // rangée 3
];

function j1() {
    var joueur1 = prompt('Nom du joueur 1');
    alert("Bienvenue " + joueur1 + "!");
}

function j2() {
    var joueur2 = prompt('Nom du joueur 2');
    alert("Bienvenue " + joueur2 + "!");
}


// var tab = [0,1];
var cases = 0;
var tours = 0;
var joueur1 = "";
var joueur2 = "";
var joueur = "O";

// function colTab(numCol) {
//     boxes = document.getElementsByClassName("cell-" + numCol);
//     colTab(numCol);
// }

function push (line, colum) {

    // variables
    var cell= document.querySelector('.cell-' + line + '-' + colum);
    var hasDiv = cell.querySelector('div'); // de type Element

    // si deja plein, on ne fait rien
    if (hasDiv) { return; }

   if (joueur == '<div class="joueur1">X</div>') {
        joueur = '<div class="joueur2">O</div>';
        tab[line][colum] = "O";
    } else {
        joueur= '<div class="joueur1">X</div>';
        tab[line][colum] = "X";
    }
    cell.innerHTML = joueur;

    checkWinner();
}

function checkWinner() {
    var winner = false;

    // premiere ligne
    if (tab[0][0] === tab[0][1] && tab[0][1] === tab[0][2] && tab[0][2] !==0) {
        winner = tab[0][0];
    }

    // deuxieme ligne
    if (tab[1][0] === tab[1][1] && tab[1][1] === tab[1][2] && tab[1][2] !==0) {
        winner = tab[1][0];
    }

    // troisieme ligne
    if (tab[2][0] === tab[2][1] && tab[2][1] === tab[2][2] && tab[2][2] !==0) {
        winner = tab[2][0];
    }

    // premiere colonne
    if (tab[0][0] === tab[1][0] && tab[1][0] === tab[2][0] && tab[2][0] !==0) {
        winner = tab[0][0];
    }

    // deuxieme colonne
    if( tab[0][1] === tab[1][1] && tab[1][1] === tab[2][1] && tab[2][1] !==0){
        winner = tab[0][1];
    }
    // troisième colonne
    if(tab[0][2] === tab[1][2] && tab[1][2] === tab[2][2] && tab[2][2] !==0){
        winner = tab[0][2];
        }

    // diagonale
    if (tab[0][0] === tab[1][1] && tab[1][1] === tab[2][2] && tab [2][2] !==0) {
        winner = tab[0][0];
    }

    // diagonale 2
    if(tab[2][0] === tab[1][1] && tab[1][1] === tab[0][2] && tab [0][2] !==0) {
        winner = tab[0][0];
    }

    // message
    if (winner) {
        alert('Le joueur '+ winner +' a gagné');
    }
}